# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'myui.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(720, 335)
        Form.setMinimumSize(QtCore.QSize(720, 335))
        Form.setMaximumSize(QtCore.QSize(720, 335))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("socket.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Form.setWindowIcon(icon)
        self.textBrowser_recv = QtGui.QTextBrowser(Form)
        self.textBrowser_recv.setGeometry(QtCore.QRect(10, 10, 520, 240))
        self.textBrowser_recv.setStyleSheet(_fromUtf8("font: 12pt \"微软雅黑\";"))
        self.textBrowser_recv.setObjectName(_fromUtf8("textBrowser_recv"))
        self.pushButton_connect = QtGui.QPushButton(Form)
        self.pushButton_connect.setGeometry(QtCore.QRect(570, 190, 111, 41))
        self.pushButton_connect.setObjectName(_fromUtf8("pushButton_connect"))
        self.pushButton_clear = QtGui.QPushButton(Form)
        self.pushButton_clear.setGeometry(QtCore.QRect(570, 270, 111, 41))
        self.pushButton_clear.setObjectName(_fromUtf8("pushButton_clear"))
        self.pushButton_send = QtGui.QPushButton(Form)
        self.pushButton_send.setGeometry(QtCore.QRect(460, 280, 71, 31))
        self.pushButton_send.setObjectName(_fromUtf8("pushButton_send"))
        self.lineEdit_send = QtGui.QLineEdit(Form)
        self.lineEdit_send.setGeometry(QtCore.QRect(10, 280, 421, 31))
        self.lineEdit_send.setObjectName(_fromUtf8("lineEdit_send"))
        self.label = QtGui.QLabel(Form)
        self.label.setGeometry(QtCore.QRect(550, 10, 51, 21))
        self.label.setObjectName(_fromUtf8("label"))
        self.label_2 = QtGui.QLabel(Form)
        self.label_2.setGeometry(QtCore.QRect(550, 100, 61, 16))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.lineEdit_ip = QtGui.QLineEdit(Form)
        self.lineEdit_ip.setGeometry(QtCore.QRect(550, 50, 151, 31))
        self.lineEdit_ip.setObjectName(_fromUtf8("lineEdit_ip"))
        self.lineEdit_port = QtGui.QLineEdit(Form)
        self.lineEdit_port.setGeometry(QtCore.QRect(550, 130, 151, 31))
        self.lineEdit_port.setObjectName(_fromUtf8("lineEdit_port"))

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Form", "PyQt socket", None))
        self.pushButton_connect.setText(_translate("Form", "连接", None))
        self.pushButton_clear.setText(_translate("Form", "清空", None))
        self.pushButton_send.setText(_translate("Form", "发送", None))
        self.lineEdit_send.setText(_translate("Form", "Socket", None))
        self.label.setText(_translate("Form", "IP地址：", None))
        self.label_2.setText(_translate("Form", "连接端口：", None))
        self.lineEdit_ip.setText(_translate("Form", "192.168.31.178", None))
        self.lineEdit_port.setText(_translate("Form", "8889", None))

